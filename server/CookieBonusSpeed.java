public class CookieBonusSpeed extends CookieEffect {
	public static final float BONUS_SPEED = 1.15f;

	public void apply(Spaceship ship) {
		ship.setSpeed(ship.getSpeed() * BONUS_SPEED);
	}

	public void unapply(Spaceship ship) {
		ship.setSpeed(ship.getSpeed() / BONUS_SPEED);
	}
}