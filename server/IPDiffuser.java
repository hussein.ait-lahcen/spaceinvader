import java.net.*;
import java.io.*;
import java.util.*;

public class IPDiffuser extends Thread {

	private static IPDiffuser Instance;
	public static final IPDiffuser getInstance() {
		if(Instance == null)
			Instance = new IPDiffuser();
		return Instance;
	}

	public void run() {
		byte[] buffer = null;
		MulticastSocket socket = null; 
		InetAddress group = null;
		DatagramPacket packet = null;
		try {
			buffer = (getLanIp() + ":" + 12345).getBytes();
			socket = new MulticastSocket(23456);
        	group = InetAddress.getByName("224.0.0.1");
        	packet = new DatagramPacket(buffer, buffer.length, group, 23456);
    	}
    	catch(Exception e) {
    		Helper.log(e);
		}

	    Helper.log("IPDiffuser started");

    	while (true) {
    		try {
	            socket.send(packet);
	            try {
	                Thread.sleep(5000);
	            } 
	            catch (InterruptedException e) {
	            }
	        }
	        catch(Exception ex) {
    			Helper.log(ex);
	        }
	    }
	}



	public static String getLanIp(){
	    String ipAddress = null;
	    Enumeration<NetworkInterface> net = null;
	    try {
	        net = NetworkInterface.getNetworkInterfaces();
	    } catch (SocketException e) {
	        throw new RuntimeException(e);
	    }

	    while(net.hasMoreElements()){
	        NetworkInterface element = net.nextElement();
	        Enumeration<InetAddress> addresses = element.getInetAddresses();
	        while (addresses.hasMoreElements()){
	            InetAddress ip = addresses.nextElement();
	            if (ip instanceof Inet4Address){

	                if (ip.isSiteLocalAddress()){

	                    ipAddress = ip.getHostAddress();
	                }

	            }

	        }
	    }
	    return ipAddress;
	}
}