import java.awt.geom.Rectangle2D;

public class Projectile extends Entity {

	private float m_speed;
	private String m_ownerId;

	public Projectile(String id, String ownerId, int teamId) {
		super(Entity.ENTITY_PROJECTILE, id, teamId, Entity.PROJECTILE_UPDATE_INTERVAL);
		m_speed = 30f;
		m_ownerId = ownerId;
	}

	public String getOwnerId() {
		return m_ownerId;
	}

	public float getSpeed() {
		return m_speed;
	}

	public void setSpeed(float speed) {
		m_speed = speed;
	}

	public void tick(long delta) {
		setY(getY() + (getDirection() * m_speed * (float)delta * 0.001f));
	}

	public Rectangle2D getBounds() {
    	float aX = getX() - (getWidth() / 2.0f);
        float aY = getY() - (getHeight() / 2.0f);
        float aWidth = getWidth();
        float aHeight = getHeight();

        return new Rectangle2D.Float(aX, aY, aWidth, aHeight);
	}
}