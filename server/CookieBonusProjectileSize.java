public class CookieBonusProjectileSize extends CookieEffect {
	public static final float BONUS_PROJ_SIZE = 1.5f;

	public void apply(Spaceship ship) {
		ship.setProjectileWidth(ship.getProjectileWidth() * BONUS_PROJ_SIZE);
		//ship.setProjectileHeight(ship.getProjectileHeight() * BONUS_PROJ_SIZE);
	}

	public void unapply(Spaceship ship) {
		ship.setProjectileWidth(ship.getProjectileWidth() / BONUS_PROJ_SIZE);
		//ship.setProjectileHeight(ship.getProjectileHeight() / BONUS_PROJ_SIZE);
	}
}