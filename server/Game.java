import java.util.*;

public class Game extends Thread {
	
	private static final Random Rand = new Random();

	public static final int MAX_Y = 65;
	public static final int MAX_X = 65;
	public static final int MIN_Y = -65;
	public static final int MIN_X = -65;

	public static final int MAX_COOKIE_Y = 40;
	public static final int MAX_COOKIE_X = 40;
	public static final int MIN_COOKIE_Y = -40;
	public static final int MIN_COOKIE_X = -40;
	public static final long COOKIE_GENERATION_INTERVAL = 2500;

	public static final int TEAM_FIRST = 0;
	public static final int TEAM_SECOND = 1;
	public static final int TEAM_NEUTRAL = 2;

	public static final float[] TEAM_COOKIE_COLOR = { 0f, 0f, 1f };
	public static final float[] TEAM_FIRST_COLOR = { 1f, 0f, 0f };
	public static final float[] TEAM_SECOND_COLOR = { 0f, 1f, 0f };

	public static final int STATE_WAITING = 0;
	public static final int STATE_SPAWN = 1;
	public static final int STATE_PLAYING = 2;
	public static final int STATE_END = 3;

	public static final int SHIP_BASE_WIDTH = 5;
	public static final int SHIP_BASE_HEIGHT = 5;

	private ArrayList<Spaceship> m_team1, m_team2;
	private ArrayList<Client> m_players;
	private HashMap<String, Entity> m_entities;
	private int m_state;
	private long m_lastUpdate;
	private long m_nextCookieGen;
	private Set<String> m_entitiesToRemove;
	private boolean m_scoreUpdated;

	public Game() {

		m_entities = new HashMap<String, Entity>();
		m_team1 = new ArrayList<Spaceship>();
		m_team2 = new ArrayList<Spaceship>();
		m_players = new ArrayList<Client>();
		m_entitiesToRemove = new HashSet<String>();

		m_state = Game.STATE_WAITING;
	}

	public int getGameState() {
		return m_state;
	}

	public boolean isOnline(String pseudo) {
		synchronized(m_players) {
			for(Client client : m_players) {
				if(client.getPseudo().equals(pseudo)) {
					return true;
				}
			}
		}
		return false;
	}

	public Spaceship getShip(String id) {
		synchronized(m_entities) {
			if(!m_entities.containsKey(id))
				return null;
			return (Spaceship)m_entities.get(id);
		}	
	}

	public void addPlayer(Client player) {
		synchronized(m_players) {
			m_players.add(player);
		}

		if(m_state != STATE_WAITING) {
			sendUnitCreation(player);
		}

		Helper.log("new player joined the game : " + player.getPseudo());
	}

	public void removePlayer(Client player) {
		synchronized(m_players) {
			m_players.remove(player);		
		}

		synchronized(m_entities) {
			Spaceship ship = player.getShip();
			if(ship != null) {
				m_entitiesToRemove.add(ship.getId());
				switch(ship.getTeamId()) {
					case TEAM_FIRST:
						m_team1.remove(ship);
						break;

					case TEAM_SECOND:
						m_team2.remove(ship);
						break;
				}
			}
		}
	}

	public void updateShipPosition(String id, float x, float y, float z) {
		synchronized(m_entities) {
			if(!m_entities.containsKey(id))
				return;
			Spaceship ship = (Spaceship)m_entities.get(id);
			ship.setX(ship.getX() + (x * ship.getSpeed()));
			ship.setY(ship.getY() + (y * ship.getSpeed()));
			ship.setZ(ship.getZ() + (z * ship.getSpeed()));
			if(ship.getX() > MAX_X)
				ship.setX(MAX_X);
			else if(ship.getX() < MIN_X)
				ship.setX(MIN_X);
			if(ship.getY() > MAX_Y)
				ship.setY(MAX_Y);
			else if(ship.getY() < MIN_Y)
				ship.setY(MIN_Y);
		}
	}

	public void createProjectile(String id) {
		synchronized(m_entities) {
			if(!m_entities.containsKey(id))
				return;
			Spaceship ship = (Spaceship)m_entities.get(id);
			if(!ship.canFire())
				return;
			ship.fire();
		}		
	}

	public void addProjectile(Projectile proj) {
		synchronized(m_entities) {
			m_entities.put(proj.getId(), proj);
			sendUnitCreation(proj);
		}
	}

	public Spaceship createShip(String id, int teamId) {
		Spaceship ship = new Spaceship(this, id, teamId);
		switch(teamId) {
			case Game.TEAM_FIRST:
				ship.setColor(TEAM_FIRST_COLOR);
				ship.setY(50);
				ship.setDirection(-1);
				m_team1.add(ship);
				break;
			case Game.TEAM_SECOND:
				ship.setColor(TEAM_SECOND_COLOR);
				ship.setY(-50);
				ship.setDirection(1);
				m_team2.add(ship);
				break;
		}
		ship.setWidth(SHIP_BASE_WIDTH);
		ship.setHeight(SHIP_BASE_HEIGHT);
		synchronized(m_entities) {	
			m_entities.put(id, ship);
		}
		Helper.log("ship created : " + id);
		return ship;
	}

	public int getNextTeamId() {
		if(m_team1.size() > m_team2.size())
			return Game.TEAM_SECOND;
		return Game.TEAM_FIRST;
	}

	public void run() {
		while(true) {
			switch(m_state) {
				case Game.STATE_WAITING: 
					checkPlayability();
					break;

				case Game.STATE_SPAWN: 
					Helper.log("[Game] started");
					spawnShips();
					break;

				case Game.STATE_PLAYING:					
					gameLoop();
					break;

				case Game.STATE_END:
					Helper.log("[Game] ended");
					gameEnd();
					break;
			}

			try {
				Thread.sleep(1);
			}
			catch(Exception e) {
				Helper.log(e);
			}
		}
	}

	private void checkPlayability() {
		synchronized(m_players) {
			if(m_players.size() >= 2) {
				m_state = Game.STATE_SPAWN;
			}
		}
	}

	private void spawnShips() {
		m_state = Game.STATE_PLAYING;

		m_entities.clear();
		m_entitiesToRemove.clear();
		m_team1.clear();
		m_team2.clear();

		synchronized(m_players) {
			for(Client player : m_players)
				player.setShip(createShip(player.getIp(), getNextTeamId()));
		}

		Server.getInstance().dispatch(Constants.SERVER_GAME_START);
		
		sendPlayersScores();
		sendUnitCreation();
	}

	private String buildUnitsPacket() {
		StringBuilder sb = new StringBuilder();
		sb.append(Constants.SERVER_UNIT_CREATE);
		synchronized(m_entities) {
			for(Entity entity : m_entities.values()) {
				sb.append(buildUnitPacket(entity));	
			}
		}
		return sb.toString();
	}

	private String buildUnitPacket(Entity entity) {
		StringBuilder sb = new StringBuilder();
		sb.append("|");
		sb.append(entity.getId()).append(";");
		sb.append(entity.getType()).append(";");
		sb.append(entity.getTeamId()).append(";");			
		sb.append(entity.getX()).append(";");	
		sb.append(entity.getY()).append(";");	
		sb.append(entity.getZ()).append(";");
		sb.append(entity.getWidth()).append(";");	
		sb.append(entity.getHeight()).append(";");
		sb.append(entity.getDirection()).append(";");	
		sb.append(entity.getColor()[0]).append(";");	
		sb.append(entity.getColor()[1]).append(";");	
		sb.append(entity.getColor()[2]).append(";");	
		if(entity instanceof Projectile) {
			sb.append(((Projectile)entity).getSpeed() + 0.05);
		}	
		else if(entity instanceof Spaceship) {
			sb.append(((Spaceship)entity).getPseudo());
		}
		return sb.toString();
	}

	private void sendUnitCreation() {
		Server.getInstance().dispatch(buildUnitsPacket());
	}

	private void sendUnitCreation(Client client) {
		client.send(buildUnitsPacket());
	}

	private void sendUnitCreation(Entity entity) {
		Server.getInstance().dispatch(Constants.SERVER_UNIT_CREATE + buildUnitPacket(entity));
	}

	private void sendShipsPosition() {			
		StringBuilder sb = new StringBuilder();
		sb.append(Constants.SERVER_UPDATE_POSITION);
		synchronized(m_entities) {
			for(Entity entity : m_entities.values()) {
				if(entity instanceof Spaceship) {
					sb.append("|");
					sb.append(entity.getId()).append(";");
					sb.append(entity.getX()).append(";");	
					sb.append(entity.getY()).append(";");	
					sb.append(entity.getZ()).append(";");
					sb.append(entity.getWidth()).append(";");	
					sb.append(entity.getHeight()).append(";");
					sb.append(entity.getDirection());	
				}	
			}
		}
		Server.getInstance().dispatch(sb.toString());
	}

	private void sendProjectilesDestroyed() {		
		synchronized(m_entities) {	
			StringBuilder sb = new StringBuilder();
			sb.append(Constants.SERVER_UNIT_DESTROY).append("|");				
			for(String id : m_entitiesToRemove) {
				sb.append(id).append("|");
			}		
			Server.getInstance().dispatch(sb.toString());
			m_entities.keySet().removeAll(m_entitiesToRemove);			
			m_entitiesToRemove.clear();

			if(m_scoreUpdated) {
				sendPlayersScores();
				m_scoreUpdated = false;
			}
		}
	}

	private void checkCollisions() {

		synchronized(m_entities) {

			Object[] values = m_entities.values().toArray();

			Entity a = null;
			Entity b = null;

			for(int i = 0; i < values.length; i++) {

				a = (Entity)values[i];

				for(int j = i; j < values.length; j++) {

					b = (Entity)values[j];

					if(b instanceof Projectile)  {
						if(b.getX() > MAX_X 
							|| b.getX() < MIN_X
							|| b.getY() > MAX_Y
							|| b.getY() < MIN_Y) {
							m_entitiesToRemove.add(b.getId());
						} 
					}		

					if(a.getId() == b.getId())
						continue;

					if(a.getTeamId() == b.getTeamId())
						continue;

					if(m_entitiesToRemove.contains(a.getId()) || m_entitiesToRemove.contains(b.getId()))
						continue;

					if(checkCollision(a, b) || checkCollision(b, a)) {		

						// entre un projectile et un vaisseau
						if((a.getType() == Entity.ENTITY_SHIP && b.getType() == Entity.ENTITY_PROJECTILE)
							|| (a.getType() == Entity.ENTITY_PROJECTILE && b.getType() == Entity.ENTITY_SHIP)) {

							m_entitiesToRemove.add(a.getId());
							m_entitiesToRemove.add(b.getId());

							if(a instanceof Spaceship) {
								((Spaceship)a).getClient().send(Constants.SERVER_KILLED);
								Spaceship ship = getShip(((Projectile)b).getOwnerId());									
								if(ship != null)
									if(ship.getClient() != null)
										ship.getClient().incScore();
							}
							else {
								((Spaceship)b).getClient().send(Constants.SERVER_KILLED);
								Spaceship ship = getShip(((Projectile)a).getOwnerId());
								if(ship != null)
									if(ship.getClient() != null)
										ship.getClient().incScore();
							}

							m_scoreUpdated = true;
						}
						// entre un vaisseau et un cookie
						else if((a.getType() == Entity.ENTITY_SHIP && b.getType() == Entity.ENTITY_COOKIE)
							|| (a.getType() == Entity.ENTITY_COOKIE && b.getType() == Entity.ENTITY_SHIP)) {

							if(a instanceof Cookie) {
								m_entitiesToRemove.add(a.getId());
								((Spaceship)b).addCookie((Cookie)a);
							}
							else {
								m_entitiesToRemove.add(b.getId());
								((Spaceship)a).addCookie((Cookie)b);
							}

						}
						// entre un project et un cookie
						else if ((a.getType() == Entity.ENTITY_PROJECTILE && b.getType() == Entity.ENTITY_COOKIE) 
						 || (a.getType() == Entity.ENTITY_COOKIE && b.getType() == Entity.ENTITY_PROJECTILE)) {

							m_entitiesToRemove.add(a.getId());
							m_entitiesToRemove.add(b.getId());

						}
						// entre deux projectiles
						else if (a.getType() == Entity.ENTITY_PROJECTILE && b.getType() == Entity.ENTITY_PROJECTILE) {

							Entity weakestProj = weakestProjectile(a, b);
							if(weakestProj == null) {
								m_entitiesToRemove.add(a.getId());
								m_entitiesToRemove.add(b.getId());
							}
							else {
								m_entitiesToRemove.add(weakestProj.getId());
							}
						}
					}
				}
			}
		}
	}

	private Entity weakestProjectile(Entity a, Entity b) {
		if(a.getWidth() > b.getWidth())
			return b;
		else if(a.getWidth() < b.getWidth())
			return a;
		return null;
	}

	private void checkEnd() {
		if(!checkAlive(m_team1) || !checkAlive(m_team2)) {
			m_state = STATE_END;
		}
	}

	private boolean checkCollision(Entity a, Entity b) {			
		return a.getBounds().intersects(b.getBounds());
	}

	private void sendCookiesGeneration(long time) {
		if(time < m_nextCookieGen)
			return;			

		m_nextCookieGen = time + COOKIE_GENERATION_INTERVAL;
		
		Cookie cookie = Cookie.generate();
		cookie.setColor(TEAM_COOKIE_COLOR);
		cookie.setX(MIN_COOKIE_X + (Rand.nextFloat() * (MAX_COOKIE_X * 2)));
		cookie.setY(MIN_COOKIE_Y + (Rand.nextFloat() * (MAX_COOKIE_Y * 2)));

		synchronized(m_entities) {
			m_entities.put(cookie.getId(), cookie);
		}

		sendUnitCreation(cookie);
	}

	private void sendPlayersScores() {
		StringBuilder sb = new StringBuilder();
		sb.append(Constants.SERVER_SCORE).append("|");
		synchronized(m_entities) {
			for(Entity entity : m_entities.values()) {			
				if(entity instanceof Spaceship) {
					Spaceship ship = (Spaceship)entity;
					sb.append("|");
					sb.append(ship.getClient().getPseudo()).append(";");
					sb.append(ship.getClient().getScore());
				}
			}
		}
		Server.getInstance().dispatch(sb.toString());
	}

	private void gameLoop() {
		long begin = (long)(System.nanoTime() * 0.000001);
		long delta = begin - m_lastUpdate;
		m_lastUpdate = begin;

		synchronized(m_entities) {
			for(Entity entity : m_entities.values()) {
				entity.update(delta);
			}
		}

		sendCookiesGeneration(begin);
		sendShipsPosition();
		checkCollisions();
		sendProjectilesDestroyed();
		checkEnd();
	}	

	private void gameEnd() {
		synchronized(m_entities) {
			// Team 2 win
			if(!checkAlive(m_team1)) {
				for(Spaceship ship : m_team1) {
					ship.getClient().send(Constants.SERVER_LOSE);
				}
				for(Spaceship ship : m_team2) {
					ship.getClient().send(Constants.SERVER_WON);
				}
			}
			// Team 1 win
			else if(!checkAlive(m_team2)) {
				for(Spaceship ship : m_team2) {
					ship.getClient().send(Constants.SERVER_LOSE);
				}
				for(Spaceship ship : m_team1) {
					ship.getClient().send(Constants.SERVER_WON);
				}
			}
			// Draw ? impossible
			else {

			}
		}

		m_state = STATE_WAITING;
	}	

	private boolean checkAlive(ArrayList<Spaceship> team) {
		for(Spaceship ship : team)
			if(m_entities.containsKey(ship.getId()))
				return true;	
		return false;
	}
}