public class CookieBonusDoubleFire extends CookieEffect {

	public static int BONUS_PROJECTILE_COUNT = 1;

	public void apply(Spaceship ship) {
		ship.setFireProjCount(ship.getFireProjCount() + BONUS_PROJECTILE_COUNT);
	}

	public void unapply(Spaceship ship) {
		ship.setFireProjCount(ship.getFireProjCount() - BONUS_PROJECTILE_COUNT);
	}
}