import java.net.*;
import java.io.*;
import java.util.*;

public class Server {

	public static void main(String[] args) {
		IPDiffuser.getInstance().start();
		ServerInstance.getInstance().start();
	}

	public static final Random Rand = new Random();
	public static long NextCookieId = 0;

	private static Server ServerInstance;

	public static Server getInstance() {
		if(ServerInstance == null)
			ServerInstance = new Server();
		return ServerInstance;
	}

	private ArrayList<Client> m_clients;
	private Game m_game;

	public Server() {
		m_clients = new ArrayList<Client>();
		m_game = new Game();
	}

	public void start() {

		ServerSocket serverSocket = null;

		m_game.start();

		try {
           	serverSocket = new ServerSocket(12345);

           	Helper.log("[Server] listening...");

           	while(true) 
	        	connected(new Client(serverSocket.accept()));
        }
        catch (Exception e) {
           System.out.println(e);
        }   
	}

	public void dispatch(Object message) {
		synchronized(m_clients) {
			for(Client client : m_clients)
				client.send(message);	
		}	
	}

	public void process(Client client, String message) {
		String[] data = message.split("\\|");
		int messageId = Integer.parseInt(data[0]);

		switch(messageId) {
			case Constants.CLIENT_REQUEST_JOIN:	
				String pseudo = data[1];
				client.setPseudo(pseudo);
				m_game.addPlayer(client);
				break;

			case Constants.CLIENT_UPDATE_PLAYER_POSITION:
				float x = Float.parseFloat(data[1]);
				float y = Float.parseFloat(data[2]);
				float z = Float.parseFloat(data[3]);
				m_game.updateShipPosition(client.getIp(), x, y, z);
				break;

			case Constants.CLIENT_REQUEST_FIRE:
				m_game.createProjectile(client.getIp());
				break;
		}
	}

	public void connected(Client client) {
		Helper.log("[" + client.getIp() + "] connected");
		synchronized(m_clients) {
			m_clients.add(client);
		}
		client.start();
		client.send(Constants.SERVER_HELLO);
	}

	public void disconnected(Client client) {
		Helper.log("[" + client.getIp() + "] disconnected");
		synchronized(m_clients) {
			m_clients.remove(client);
		}

		m_game.removePlayer(client);
	}
}