import java.net.*;
import java.io.*;
import java.util.*;

public class Client extends Thread {

	private String m_ip;
	private DataInputStream m_in;
	private PrintStream m_out;
	private Spaceship m_ship;
	private String m_pseudo;
	private int m_score;

	public Client(Socket socket) {
		try {
			m_ip = socket.getRemoteSocketAddress().toString();
			m_in = new DataInputStream(socket.getInputStream());
        	m_out = new PrintStream(socket.getOutputStream());
    	}
    	catch(Exception e) {
			Server.getInstance().disconnected(this);
    	}
	}

	public int getScore() {
		return m_score;
	}

	public void incScore() {
		m_score++;
	}
	
	public void setScore(int score) {
		m_score = score;
	}

	public String getIp() {
		return m_ip;
	}

	public String getPseudo() {
		return m_pseudo;
	}

	public void setPseudo(String pseudo) {
		m_pseudo = pseudo;
	}

	public Spaceship getShip() {
		return m_ship;
	}

	public void setShip(Spaceship ship) {
		m_ship = ship;
		ship.setClient(this);
	}

	public void run() {
		try {
			String message = "";	
			while ((message = m_in.readLine()) != null) 
				Server.getInstance().process(this, message);
		}			
		catch(Exception e) {	
			Helper.log("Server::run : " + e);			
		}
		finally {
			Server.getInstance().disconnected(this);
		}
	}

	public void send(Object message) {
		try {
			m_out.println(message.toString() + "|");
		}
		catch(Exception e) {
			Server.getInstance().disconnected(this);				
		}
	}	
}
