import java.awt.geom.Rectangle2D;
import java.util.*;

public class Spaceship extends Entity {

	public static final long FIRE_INTERVAL = 1200;
	public static final int FIRE_PROJECTILE_COUNT = 1;

	private Client m_client;
	private long m_nextProjectileId;
	private float m_speed;
	private float m_fireInterval;
	private long m_nextFire;
	private float m_projectileWidth;
	private float m_projectileHeight;
	private ArrayList<Cookie> m_cookies;
	private int m_fireProjCount;
	private Game m_game;

	public Spaceship(Game game, String id, int teamId) {
		super(Entity.ENTITY_SHIP, id, teamId, Entity.SPACESHIP_UPDATE_INTERVAL);
		m_speed = 0.35f;
		m_fireInterval = 1f;
		m_projectileWidth = 0.5f;
		m_projectileHeight = 2f;
		m_game = game;
		m_fireProjCount = FIRE_PROJECTILE_COUNT;

		m_cookies = new ArrayList<Cookie>();
	}

	public int getFireProjCount() {
		return m_fireProjCount;
	}

	public void setFireProjCount(int projCount) {
		m_fireProjCount = projCount;
	}

	public Client getClient() {
		return m_client;
	}

	public String getPseudo() {
		if(m_client == null)
			return "?";
		return m_client.getPseudo();
	}

	public void setClient(Client client) {
		m_client = client;
	}
	
	public ArrayList<Cookie> getCookies() {
		return m_cookies;
	}

	public void addCookie(Cookie cookie) {
		m_cookies.add(cookie);
		cookie.setTimeout(System.currentTimeMillis() + cookie.getDuration());
		cookie.apply(this);
	}

	public float getProjectileWidth() {
		return m_projectileWidth;
	}

	public void setProjectileWidth(float projWidth) {
		m_projectileWidth = projWidth;
	}

	public float getProjectileHeight() {
		return m_projectileHeight;
	}

	public void setProjectileHeight(float projHeight) {
		m_projectileHeight = projHeight;
	}

	public float getSpeed() {
		return m_speed;
	}

	public float getFireInterval() {
		return m_fireInterval;
	}

	public void setFireInterval(float interval) {
		m_fireInterval = interval;
	}

	public void setSpeed(float speed) {
		m_speed = speed;
	}

	public boolean canFire() {
		return System.currentTimeMillis() > m_nextFire;
	}

	public void tick(long delta) {
		for(int i = m_cookies.size() - 1; i > -1; i--) {
			Cookie cookie = m_cookies.get(i);
			if(cookie.getTimeout() < System.currentTimeMillis()) {
				m_cookies.remove(cookie);
				cookie.unapply(this);
			}
		}
	}

	public void fire() {
		for(Cookie cookie : m_cookies) {
			cookie.applyFire(this);
		}
		if(m_fireProjCount == 1) {
			createProjectile(m_x, m_y);
		}
		else {
			float semi = 3f / m_fireProjCount;
			float step = 3f;
			for(int i = 0; i < m_fireProjCount; i++) {
				createProjectile((m_x - semi) + (i * step), m_y);
			}
		}
	}

	public void createProjectile(float x, float y) {
		m_nextFire = System.currentTimeMillis() + (long)(FIRE_INTERVAL * m_fireInterval);
		Projectile proj = new Projectile(getId() + "_" + m_nextProjectileId++, getId(), getTeamId());
		proj.setDirection(m_direction);
		proj.setWidth(m_projectileWidth);
		proj.setHeight(m_projectileHeight);
		proj.setX(x);
		proj.setY(y);
		proj.setColor(m_color);
		m_game.addProjectile(proj);
	}

	public Rectangle2D getBounds() {
    	float aX = getX() - (getWidth() / 2.0f);
        float aY = getDirection() == 1 ? getY() - getHeight() : getY();
        float aWidth = getWidth();
        float aHeight = getHeight();

        return new Rectangle2D.Float(aX, aY, aWidth, aHeight);
	}
}