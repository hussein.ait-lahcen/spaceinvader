import java.util.*;
import java.awt.geom.Rectangle2D;

public class Cookie extends Entity {
		
	private static final Random Rand = new Random();

	private static long NextCookieId = 0;

	public static final int BONUS_SPEED = 0;
	public static final int BONUS_FIRE = 1;
	public static final int BONUS_PROJ_WIDTH = 2;
	public static final int BONUS_FIRE_PROJ_COUNT = 3;

	public static final float COOKIE_WIDTH = 3f;
	public static final float COOKIE_HEIGHT = 3f;

	public static final int COOKIE_MIN_DURATION = 10000;
	public static final int COOKIE_MAX_DURATION = 25000;

	public static final int TEAM_COOKIES = 3;

	public static Cookie generate() {
		Cookie cookie = new Cookie(NextCookieId++, (int)(Rand.nextFloat() * (COOKIE_MAX_DURATION - COOKIE_MIN_DURATION)) + COOKIE_MIN_DURATION);
		int rand = Rand.nextInt(BONUS_FIRE_PROJ_COUNT + 1);
		switch(rand) {
			case BONUS_SPEED:
				cookie.addEffect(new CookieBonusSpeed());
				break;

			case BONUS_FIRE:
				cookie.addEffect(new CookieBonusFire());
				break;

			case BONUS_PROJ_WIDTH:
				cookie.addEffect(new CookieBonusProjectileSize());
				break;

			case BONUS_FIRE_PROJ_COUNT:
				cookie.addEffect(new CookieBonusDoubleFire());
				break;
		}
		return cookie;
	}

	private ArrayList<CookieEffect> m_effects;
	private long m_duration;
	private long m_timeout;

	public Cookie(long id, long duration) {
		super(Entity.ENTITY_COOKIE, id + "", TEAM_COOKIES, -1);
		m_effects = new ArrayList<CookieEffect>();
		m_width = COOKIE_WIDTH;
		m_height = COOKIE_HEIGHT;
		m_duration = duration;
	}

	public long getTimeout() {
		return m_timeout;
	}

	public void setTimeout(long timeout) {
		m_timeout = timeout;
	}

	public long getDuration() {
		return m_duration;
	}

	public void addEffect(CookieEffect effect) {
		m_effects.add(effect);
	}

	public ArrayList<CookieEffect> getEffects() {
		return m_effects;
	}

	public boolean applyCollision(Spaceship ship) {
		boolean collision = true;
		for(CookieEffect effect : m_effects) {
			collision = collision && effect.applyCollision(ship);
		}
		return collision;
	}

	public void applyFire(Spaceship ship) {
		for(CookieEffect effect : m_effects) {
			effect.applyFire(ship);
		}
	}

	public void apply(Spaceship ship) {
		for(CookieEffect effect : m_effects) {
			effect.apply(ship);
		}
	}

	public void unapply(Spaceship ship) {
		for(CookieEffect effect : m_effects) {
			effect.unapply(ship);
		}
	}

	public Rectangle2D getBounds() {
    	float aX = getX() - (getWidth() / 2.0f);
        float aY = getY() - (getHeight() / 2.0f);
        float aWidth = getWidth();
        float aHeight = getHeight();

        return new Rectangle2D.Float(aX, aY, aWidth, aHeight);
	}
}