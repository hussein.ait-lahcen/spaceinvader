public class CookieBonusFire extends CookieEffect {
	public static final float BONUS_FIRE = 0.75f;

	public void apply(Spaceship ship) {
		ship.setFireInterval(ship.getFireInterval() * BONUS_FIRE);
	}

	public void unapply(Spaceship ship) {
		ship.setFireInterval(ship.getFireInterval() / BONUS_FIRE);
	}
}