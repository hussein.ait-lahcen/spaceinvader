import java.awt.geom.Rectangle2D;

public abstract class Entity extends Updatable {

    public static final long SPACESHIP_UPDATE_INTERVAL = 10;
    public static final long PROJECTILE_UPDATE_INTERVAL = 10;
    public static final int ENTITY_SHIP = 0;
    public static final int ENTITY_PROJECTILE = 1;
    public static final int ENTITY_COOKIE = 2;

    protected int m_type;
    protected String m_id;
    protected int m_teamId;
    protected float m_x;
    protected float m_y;
    protected float m_z;
    protected float m_height;
    protected float m_width;
    protected float m_direction;
    protected float[] m_color;

    public Entity(int type, String id, int teamId, long updateInterval) {
        super(updateInterval);
        m_type = type;
        m_id = id;
        m_teamId = teamId;
    }

    public float[] getColor() {
        return m_color;
    }

    public void setColor(float[] color) {
        m_color = color;
    }

    public int getType() {
        return m_type;
    }

    public String getId() {
        return m_id;
    }

    public int getTeamId() {
        return m_teamId;
    }

    public void setTeamId(int teamId) {
        m_teamId = teamId;
    }

    public float getX() {
        return m_x;
    }

    public float getY() {
        return m_y;
    }

    public float getZ() {
        return m_z;
    }

    public float getHeight() {
        return m_height;
    }

    public void setHeight(float height) {
        m_height = height;
    }

    public float getWidth() {
        return m_width;
    }

    public void setWidth(float width) {
        m_width = width;
    }

    public float getDirection() {
        return m_direction;
    }

    public void setDirection(float direction) {
        m_direction = direction;
    }

    public void setX(float x) {
        m_x = x;
    }

    public void setY(float y) {
        m_y = y;
    }

    public void setZ(float z) {
        m_z = z;
    }

    public Rectangle2D getBounds() {
        return null;
    }
}