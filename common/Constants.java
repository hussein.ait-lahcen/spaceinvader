public class Constants {
	public static final int CLIENT_UPDATE_PLAYER_POSITION = 0;
	public static final int CLIENT_REQUEST_JOIN = 1;
	public static final int SERVER_GAME_START = 2; // pop the game screen
	public static final int SERVER_UPDATE_POSITION = 3; // list[unitId unitPos]
	public static final int SERVER_UNIT_DESTROY = 4; // list[unitId]
	public static final int SERVER_UNIT_CREATE = 5; // list[unitId unitType]
	public static final int SERVER_HELLO = 6;
	public static final int CLIENT_REQUEST_FIRE = 7;
	public static final int SERVER_LOSE = 8;	
	public static final int SERVER_KILLED = 9;
	public static final int SERVER_WON = 10;
	public static final int SERVER_SCORE = 11;
}