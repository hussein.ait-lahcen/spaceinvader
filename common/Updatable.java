import java.util.*;

public abstract class Updatable {

	private ArrayList<Updatable> m_subUpdatables;

	public Updatable(long updateInterval) {
		this();
	}

	public Updatable() {		
		m_subUpdatables = new ArrayList<Updatable>();
	}

	public void addUpdatable(Updatable updatable) {
		m_subUpdatables.add(updatable);
	}

	public void removeUpdatable(Updatable updatable) {
		m_subUpdatables.remove(updatable);
	}

	public void update(long delta) {

		tick(delta);

		for(Updatable subUpdatable : m_subUpdatables)
			subUpdatable.update(delta);
	}

	// should be overriden
	public void tick(long delta) {

	}
}