import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.util.awt.TextRenderer;
import java.awt.Color;

public class Spaceship extends DrawableObject {

    private String m_pseudo;
    private Model m_model;

    public Spaceship(String id, int teamId, float x, float y, float z, float width, float height, float direction) {
        super(Entity.ENTITY_SHIP, id, teamId, -1);            
        m_x = x;
        m_y = y;
        m_z = z;
        m_width = width;
        m_height = height;
        m_direction = direction;
    }

    public String getPseudo() {
        return m_pseudo;
    }

    public void setPseudo(String pseudo) {
        m_pseudo = pseudo;
    }

    public void display(GLU glu, GLUT glut, GL2 gl, TextRenderer textRenderer) {
        if(m_model == null) {
            m_model = ModelManager.getInstance().getModel(ModelManager.SHIP);
        }

        if(m_model == null)
            return;

        gl.glPushMatrix();
        gl.glColor3f(m_color[0], m_color[1], m_color[2]);

        gl.glTranslated(m_x,m_y-(m_height*m_direction),m_z);
        if(m_direction==-1){
            gl.glRotatef(180, 0f,0f,1f);
        }

        //VBO
        gl.glScalef(m_width*1f,m_height*1f,m_width*1f);
        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, m_model.normal_fichierVBO);
        gl.glNormalPointer(GL2.GL_FLOAT,0,0);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, m_model.sommets_fichierVBO);
        gl.glVertexPointer(3, GL2.GL_DOUBLE, 0, 0);
        gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, m_model.myindex_fichierVBO);
        gl.glDrawElements(GL2.GL_TRIANGLES, m_model.nbIndex_fichier, GL2.GL_UNSIGNED_INT, 0);
        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glPopMatrix();


        textRenderer.beginRendering(SpaceInvader.Width, SpaceInvader.Height);
        textRenderer.draw(m_id, 60 + (int)m_x, 60 + (int)m_y);
        textRenderer.endRendering();
        textRenderer.flush();
    }
}