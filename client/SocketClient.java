import java.net.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

public class SocketClient implements Runnable{
	
	private SpaceInvader m_game;
	private Socket m_socket;
	private String m_host;
	private int m_port;
    private PrintStream m_out;
	private DataInputStream m_in;
	private String m_pseudo;

	public SocketClient(SpaceInvader game, String host, int port) {
		m_host = host;
		m_port = port;
		m_game = game;
		m_pseudo = JOptionPane.showInputDialog(null, "Saisissez votre pseudo", "Selection du pseudo", JOptionPane.INFORMATION_MESSAGE);
	}

	public void start() {
		new Thread(this).start();
	}

	public void run() {
		try {

			m_game.getConsole().info("Connecting...");

			m_socket = new Socket(m_host, m_port);	
	      	m_out = new PrintStream(m_socket.getOutputStream());
		  	m_in = new DataInputStream(m_socket.getInputStream());

			m_game.getConsole().success("Connected !");

			String message = "";	 
			while ((message = m_in.readLine()) != null) {
				try {
					processMessage(message);
				}
				catch(Exception e) {
					System.out.println("SocketClient::processMessage : " + e);							
				}
			}
		}			
		catch(Exception e) {		
			m_game.getConsole().warning("Server offline.");

			System.out.println("SocketClient::run : " + e);		
			try {
	  			Thread.sleep(5000);
	  		}
	  		catch(Exception ex) {
	  		}
	  		run();
		}
		finally {
			try {
				m_socket.close();
			}
			catch(Exception e) {

			}
		}
	}

	public void send(Object message) {
		try {
			m_out.println(message.toString() + "|");
		}
		catch(Exception e) {
			System.out.println("SocketClient::send : " + e);				
		}
	}	

	public void sendMovement(float x, float y, float z) {
		send(Constants.CLIENT_UPDATE_PLAYER_POSITION + "|" + x + "|" + y + "|" + z);
	}

	public void sendFire() {
		send(Constants.CLIENT_REQUEST_FIRE);
	}

	private void processMessage(String message) {

		// ID|DONNEES
		String[] data = message.split("\\|");
		int messageId = Integer.parseInt(data[0]);

		switch(messageId) {
			case Constants.SERVER_HELLO:			
				send(Constants.CLIENT_REQUEST_JOIN + "|" + m_pseudo);
				break;

			case Constants.SERVER_GAME_START:
				m_game.getConsole().success("Game started.");
				m_game.getEntityMgr().clear();

				SoundManager.getInstance().playSound(SoundManager.PREPARE_FOR_BATTLE);
				break;


			case Constants.SERVER_KILLED:
				m_game.getConsole().error("You are dead.");
        		m_game.getConsole().info("Press [c] to spectate a ship.");
				break;

			case Constants.SERVER_WON:
				m_game.getConsole().success("Win.");
				m_game.getConsole().addPoint();
				break;

			case Constants.SERVER_LOSE:
				m_game.getConsole().error("Lost.");
				m_game.getConsole().addOpponentPoint();
				break;

			case Constants.SERVER_UPDATE_POSITION:
				for(int i = 1; i < data.length; i++) {
					if(data[i].equals(""))
						continue;

					String[] entityData = data[i].split(";");

					String entityId = entityData[0];
					float x = Float.parseFloat(entityData[1]);
					float y = Float.parseFloat(entityData[2]);
					float z = Float.parseFloat(entityData[3]);
					float width = Float.parseFloat(entityData[4]);
					float height = Float.parseFloat(entityData[5]);
					float direction = Float.parseFloat(entityData[6]);

					m_game.getEntityMgr().updatePosition(entityId, x, y, z, width, height, direction);
				}
				break;

			case Constants.SERVER_UNIT_DESTROY:
				for(int i = 1; i < data.length; i++) {
					if(data[i].equals(""))
						continue;

					m_game.getEntityMgr().destroyEntity(data[i]);
				}
				break;

			case Constants.SERVER_UNIT_CREATE:
				for(int i = 1; i < data.length; i++) {
					if(data[i].equals(""))
						continue;

					String[] entityData = data[i].split(";");

					m_game.getEntityMgr().createEntity(entityData);
				}
				break;

			case Constants.SERVER_SCORE:
				m_game.getConsole().clearPlayerScores();
				for(int i = 1; i < data.length; i++) {
					if(data[i].equals(""))
						continue;

					String[] scoreData = data[i].split(";");
					String playerId = scoreData[0];
					int playerScore = Integer.parseInt(scoreData[1]);

					m_game.getConsole().setPlayerScore(playerId, playerScore);
				}
				break;
		}
	}
}