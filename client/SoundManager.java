import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SoundManager {

	private static final String SOUND_PATH = "./sound";	
	public static final String PREPARE_FOR_BATTLE = SOUND_PATH + "/prepare.wav";
	public static final String FIRST_BLOOD = SOUND_PATH + "/firstblood.wav";
	public static final String RAMPAGE = SOUND_PATH + "/rampage.wav";
	public static final String HOLY_SHIT = SOUND_PATH + "/holyshit.wav";
	public static final String GODLIKE = SOUND_PATH + "/godlike.wav";

	private static SoundManager instance;

	public static SoundManager getInstance() {
		if(instance == null)
			instance = new SoundManager();
		return instance;
	}

	private ExecutorService m_executor;

	public SoundManager() {
		m_executor = Executors.newFixedThreadPool(5);
	}

	public void playSound(String file) {
		m_executor.execute(new SoundPlay(file));
	}
}