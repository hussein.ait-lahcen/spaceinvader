import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.util.awt.TextRenderer;
import java.awt.Color;

public final class Cookie extends DrawableObject {

    public Cookie(String id, int teamId, float x, float y, float z, float width, float height, float[] colors) {
        super(Entity.ENTITY_COOKIE, id, teamId, 10);
        m_x = x;
        m_y = y;
        m_z = z;
        m_color = colors;
        m_width = width;
        m_height = height;
    }

    public void display(GLU glu, GLUT glut, GL2 gl, TextRenderer textRenderer){
        gl.glPushMatrix();
        gl.glColor3f(m_color[0], m_color[1], m_color[2]);
        gl.glTranslated(m_x, m_y, m_z);
        gl.glScalef(getWidth(), getHeight(), getWidth());
        glut.glutSolidSphere(0.5f, 10, 10);
        gl.glPopMatrix();
    }
}