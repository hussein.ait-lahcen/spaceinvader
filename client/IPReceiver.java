import java.net.*;
import java.io.*;
import java.util.*;

public class IPReceiver extends Thread {
	private static IPReceiver Instance;
	public static final IPReceiver getInstance() {
		if(Instance == null)
			Instance = new IPReceiver();
		return Instance;
	}

	private String m_ip;
	private int m_port;

	public String getIp() {
		return m_ip;
	}

	public int getPort() {
		return m_port;
	}

	public void run() {
		try {
			MulticastSocket socket = new MulticastSocket(23456);
			InetAddress group = InetAddress.getByName("224.0.0.1");
			socket.joinGroup(group);

			DatagramPacket packet;
		    byte[] buf = new byte[256];
			packet = new DatagramPacket(buf, buf.length);

			while(m_ip == null) {
				socket.receive(packet);
				String message = new String(packet.getData());
				String[] data = message.split(":");
				m_ip = data[0];
				m_port = Integer.parseInt(data[1].trim());
			}

			Helper.log("IPReceiver host received : " + m_ip + ":" + m_port);

			socket.leaveGroup(group);
			socket.close();
		}
		catch(Exception e) {
			Helper.log(e);
		}
	}
}