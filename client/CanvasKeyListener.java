import java.awt.event.*;

import javax.media.opengl.awt.GLCanvas;

public class CanvasKeyListener implements KeyListener {
	private SpaceInvader m_space;
	private GLCanvas m_canvas;

	public CanvasKeyListener(GLCanvas canvas, SpaceInvader space) {
		m_space = space;
		m_canvas = canvas;
	}

	public void keyTyped(KeyEvent e) {

	}

	public void keyPressed(KeyEvent e) {        
        float x = 0, y = 0, z = 0;
	    switch(e.getKeyChar()) {	    	
	        case 'z':
	        	m_space.getEntityMgr().setMoveY(1f);
	        	break;

	        case 's':
	        	m_space.getEntityMgr().setMoveY(-1f);
	        	break;

	        case 'd':
	        	m_space.getEntityMgr().setMoveX(1f);
	        	break;

	        case 'q':
	        	m_space.getEntityMgr().setMoveX(-1f);
	        	break;

	        case 'c':
	        	m_space.getEntityMgr().switchCameraFocus();
	       		break;

	        case 'v':
	        	m_space.getEntityMgr().cancelCameraFocus();
	        	break;

	        case KeyEvent.VK_SPACE:
	        	m_space.getEntityMgr().setFire(true);
	        	break;
	    }
	}

	public void keyReleased(KeyEvent e) {
		switch(e.getKeyChar()) {
	        case 'z':
	        	m_space.getEntityMgr().setMoveY(0f);
	        	break;

	        case 's':
	        	m_space.getEntityMgr().setMoveY(0f);
	        	break;

	        case 'd':
	        	m_space.getEntityMgr().setMoveX(0f);
	        	break;

	        case 'q':
	        	m_space.getEntityMgr().setMoveX(0f);
	        	break;

	        case KeyEvent.VK_SPACE:
	        	m_space.getEntityMgr().setFire(false);
	        	break;
	    }
	}
}