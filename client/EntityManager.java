import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.awt.TextRenderer;
import java.awt.Color;
import com.jogamp.opengl.util.gl2.GLUT;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import java.util.*;
import java.io.*;

import javax.media.opengl.awt.GLCanvas;

public class EntityManager extends Thread {

    private SpaceInvader m_space;
    private HashMap<String, DrawableObject> m_entities;
    private long m_lastUpdate;
    private long m_updateTime;
    private float m_moveX;
    private float m_moveY;
    private float m_moveZ;
    private boolean m_fire;
    private Spaceship m_focus;
    private Spaceship m_localShip;
    private int m_focusIndex;

    public EntityManager(SpaceInvader space) {
        m_entities = new HashMap<String, DrawableObject>();
        m_space = space;
    }

    public void setFire(boolean fire) {
        m_fire = fire;
    }

    public void setMoveX(float x) {
        m_moveX = x;
    }

    public void setMoveY(float y) {
        m_moveY = y;
    }

    public void setMoveZ(float z) {
        m_moveZ = z;
    }

    public void clear() {
        synchronized(m_entities) {
            m_entities.clear();
        }

        m_focusIndex = 0;
        m_focus = null;
    }

    public Entity getEntity(String playerId) {
        synchronized(m_entities) {
            if(!m_entities.containsKey(playerId))
                return null;
            return m_entities.get(playerId);
        }
    }

    public void run() {        
        while(true) {
            long begin = System.currentTimeMillis();
            long delta = begin - m_lastUpdate;
            m_lastUpdate = begin;

            synchronized(m_entities) {
                for(Entity entity : m_entities.values()) {
                    entity.update(delta);
                }
            }

            if(m_localShip != null) {
                if(m_moveX != 0f || m_moveY != 0f || m_moveZ != 0f)
                    m_space.move(m_moveX * m_localShip.getDirection(), m_moveY * m_localShip.getDirection(), m_moveZ * m_localShip.getDirection());

                if(m_fire)
                    m_space.fire();
            }

            try {
                Thread.sleep(10);
            }
            catch(Exception e) {
            }
        }
    }

    public void cancelCameraFocus() {
        m_focus = null;        
    }

    public void switchCameraFocus() {
        synchronized(m_entities) {
            int i = 0;        
            for(Entity entity : m_entities.values()) {
                if(entity instanceof Spaceship) {
                    if(i == m_focusIndex) {
                        m_focus = (Spaceship)entity;
                        m_focusIndex = i;
                    }
                    i++;
                }
            }
            m_focusIndex = (m_focusIndex + 1) % i;
        }
    }

    public void updatePosition(String entityId, float x, float y, float z, float width, float height, float direction) {        
        Entity entity = null;
        synchronized(m_entities) {
            if(!m_entities.containsKey(entityId)) {
                return;
            }
            entity = m_entities.get(entityId);
        }    
        entity.setX(x);
        entity.setY(y);
        entity.setZ(z);
        entity.setWidth(width);
        entity.setHeight(height);
        entity.setDirection(direction);
    }

    public void destroyEntity(String id) {
        synchronized(m_entities) {
            if(!m_entities.containsKey(id))
                return;
            m_entities.remove(id);
            if(m_focus != null && m_focus.getId().equals(id)) {
                switchCameraFocus();
            }
        }
    }

    public void createEntity(String[] entityData) {        
        String entityId = entityData[0];
        int entityType = Integer.parseInt(entityData[1]);
        int teamId = Integer.parseInt(entityData[2]);
        float x = Float.parseFloat(entityData[3]);
        float y = Float.parseFloat(entityData[4]);
        float z = Float.parseFloat(entityData[5]);
        float width = Float.parseFloat(entityData[6]);
        float height = Float.parseFloat(entityData[7]);
        float direction = Float.parseFloat(entityData[8]);
        float colorR = Float.parseFloat(entityData[9]);
        float colorG = Float.parseFloat(entityData[10]);
        float colorB = Float.parseFloat(entityData[11]);
        float[] colors = new float[] {colorR, colorG, colorB };

        switch(entityType) {
            case Entity.ENTITY_SHIP:
                String pseudo = entityData[12];
                createShip(entityId, teamId, x, y, z, width, height, direction, colors, pseudo);
                break;

            case Entity.ENTITY_PROJECTILE:                         
                float speed = Float.parseFloat(entityData[12]);
                createProjectile(entityId, teamId, x, y, z, width, height, direction, colors, speed);
                break;

            case Entity.ENTITY_COOKIE:
                createCookie(entityId, teamId, x, y, z, width, height, colors);
                break;
        }
    }

    public void createCookie(String id, int teamId, float x, float y, float z, float width, float height, float[] colors) {
        synchronized(m_entities) {
            m_entities.put(id, new Cookie(id, teamId, x, y, z, width, height, colors));
        }
    }

    public void createShip(String id, int teamId, float x, float y, float z, float width, float height, float direction, float[] colors, String pseudo) {
        synchronized(m_entities) {
            Spaceship ship = new Spaceship(id, teamId, x, y, z, width, height, direction);
            ship.setColor(colors);
            ship.setPseudo(pseudo);
            if(id.substring(1).split(":")[0].equals(Helper.getLanIp())) {
                m_localShip = ship;
                m_localShip.setColor(new float[] { 1f, 1f, 1f });
            }
            m_entities.put(id, ship);
        }
    }

    public void createProjectile(String id, int teamId, float x, float y, float z, float width, float height, float direction, float[] colors, float speed) {
        synchronized(m_entities) {
            m_entities.put(id, new Projectile(id, teamId, x, y, z, width, height, direction, colors, speed));
        }
    }

    public void display(GLU glu, GLUT glut, GL2 gl, TextRenderer textRenderer) {        
        //gl.glLightiv(GL2.GL_LIGHT0, GL2.GL_POSITION, new int[] { 0, 0, 100, 1 }, 0);
        if(m_focus != null) {
            glu.gluLookAt(m_focus.getX(), m_focus.getY() + (m_focus.getDirection() * -40), m_focus.getWidth() + 15, 
            m_focus.getX(), m_focus.getY() + (m_focus.getDirection() * 30), 0,  // look origin
            0, 0, 1); // y top vector
        }
        else  {
            glu.gluLookAt(0, 0, 90, 
            0, 0, 0,  // look origin
            0, m_localShip == null ? 1 : m_localShip.getDirection(), 0); // y top vector
        }
        synchronized(m_entities) {
            for(DrawableObject entity : m_entities.values()) {
                entity.display(glu, glut, gl, textRenderer);
            } 
        }
    }
}