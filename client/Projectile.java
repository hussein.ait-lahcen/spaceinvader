import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.util.awt.TextRenderer;
import java.awt.Color;

public class Projectile extends DrawableObject {

    private float m_speed;

    public Projectile(String id, int teamId, float x, float y, float z, float width, float height, float direction, float[] colors, float speed) {
        super(Entity.ENTITY_PROJECTILE, id, teamId, 10);
        m_x = x;
        m_y = y;
        m_z = z;
        m_color = colors;
        m_width = width;
        m_height = height;
        m_direction = direction;
        m_speed = speed;
    }

    public float getSpeed() {
        return m_speed;
    }

    public void setSpeed(float speed) {
        m_speed = speed;
    }

    public void display(GLU glu, GLUT glut, GL2 gl, TextRenderer textRenderer){
        gl.glPushMatrix();
        gl.glColor3f(m_color[0], m_color[1], m_color[2]);
        gl.glTranslated(m_x, m_y, m_z);
        gl.glScalef(getWidth(), getHeight(), 1f);
        glut.glutSolidSphere(0.5f, 10, 10);
        gl.glPopMatrix();
    }

    public void tick(long delta) {            
        setY(getY() + (getDirection() * m_speed * (float)delta * 0.001f));
    }
}