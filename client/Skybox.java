import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.gl2.GLUT;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import java.io.*;

import javax.media.opengl.awt.GLCanvas;

public class Skybox {
	
	private Texture m_texture;

	public Skybox(String path){
		loadTexture(path);
	}

	public void display(GL2 gl){

		   // Store the current matrix
	    gl.glPushMatrix();

   		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);
    	gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP);
	 
	    // Reset and transform the matrix.
	    gl.glLoadIdentity();
	 
	    // Enable/Disable features
	    gl.glPushAttrib(GL2.GL_ENABLE_BIT);
	    gl.glEnable(GL2.GL_TEXTURE_2D);
	    gl.glDisable(GL2.GL_DEPTH_TEST);
	    gl.glDisable(GL2.GL_LIGHTING);
	    gl.glDisable(GL2.GL_BLEND);
	 
	    // Just in case we set all vertices to white.
	    gl.glColor4f(1,1,1,1);
	 
	    // Render the front quad
	    m_texture.bind(gl);
	    gl.glBegin(GL2.GL_QUADS);
	        gl.glTexCoord2f(0, 0); gl.glVertex3f(  0.5f, -0.5f, -0.5f );
	        gl.glTexCoord2f(1, 0); gl.glVertex3f( -0.5f, -0.5f, -0.5f );
	        gl.glTexCoord2f(1, 1); gl.glVertex3f( -0.5f,  0.5f, -0.5f );
	        gl.glTexCoord2f(0, 1); gl.glVertex3f(  0.5f,  0.5f, -0.5f );
	    gl.glEnd();
	 
	    // Render the left quad
	    gl.glBegin(GL2.GL_QUADS);
	        gl.glTexCoord2f(0, 0); gl.glVertex3f(  0.5f, -0.5f,  0.5f );
	        gl.glTexCoord2f(1, 0); gl.glVertex3f(  0.5f, -0.5f, -0.5f );
	        gl.glTexCoord2f(1, 1); gl.glVertex3f(  0.5f,  0.5f, -0.5f );
	        gl.glTexCoord2f(0, 1); gl.glVertex3f(  0.5f,  0.5f,  0.5f );
	    gl.glEnd();
	 
	    // Render the back quad
	    gl.glBegin(GL2.GL_QUADS);
	        gl.glTexCoord2f(0, 0); gl.glVertex3f( -0.5f, -0.5f,  0.5f );
	        gl.glTexCoord2f(1, 0); gl.glVertex3f(  0.5f, -0.5f,  0.5f );
	        gl.glTexCoord2f(1, 1); gl.glVertex3f(  0.5f,  0.5f,  0.5f );
	        gl.glTexCoord2f(0, 1); gl.glVertex3f( -0.5f,  0.5f,  0.5f );
	 
	    gl.glEnd();
	 
	    // Render the right quad
	    gl.glBegin(GL2.GL_QUADS);
	        gl.glTexCoord2f(0, 0); gl.glVertex3f( -0.5f, -0.5f, -0.5f );
	        gl.glTexCoord2f(1, 0); gl.glVertex3f( -0.5f, -0.5f,  0.5f );
	        gl.glTexCoord2f(1, 1); gl.glVertex3f( -0.5f,  0.5f,  0.5f );
	        gl.glTexCoord2f(0, 1); gl.glVertex3f( -0.5f,  0.5f, -0.5f );
	    gl.glEnd();
	 
	    // Render the top quad
	    gl.glBegin(GL2.GL_QUADS);
	        gl.glTexCoord2f(0, 1); gl.glVertex3f( -0.5f,  0.5f, -0.5f );
	        gl.glTexCoord2f(0, 0); gl.glVertex3f( -0.5f,  0.5f,  0.5f );
	        gl.glTexCoord2f(1, 0); gl.glVertex3f(  0.5f,  0.5f,  0.5f );
	        gl.glTexCoord2f(1, 1); gl.glVertex3f(  0.5f,  0.5f, -0.5f );
	    gl.glEnd();
	 
	    // Render the bottom quad
	    gl.glBegin(GL2.GL_QUADS);
	        gl.glTexCoord2f(0, 0); gl.glVertex3f( -0.5f, -0.5f, -0.5f );
	        gl.glTexCoord2f(0, 1); gl.glVertex3f( -0.5f, -0.5f,  0.5f );
	        gl.glTexCoord2f(1, 1); gl.glVertex3f(  0.5f, -0.5f,  0.5f );
	        gl.glTexCoord2f(1, 0); gl.glVertex3f(  0.5f, -0.5f, -0.5f );
	    gl.glEnd();
	 
	    // Restore enable bits and matrix
	    gl.glPopAttrib();
	    gl.glPopMatrix();

	}

	public void loadTexture(String path){
		try{
			m_texture = TextureIO.newTexture(new File(path), true);
		}
		catch (Exception e) {
			System.out.println(e.toString());
		}
	}
}