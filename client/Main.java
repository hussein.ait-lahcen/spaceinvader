
import javax.media.opengl.*;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import javax.swing.*;

import com.jogamp.opengl.util.FPSAnimator;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class Main extends JFrame {

	private static final long serialVersionUID = 1L;

	public Main(String host, int port) {
		
		setTitle("SpaceInvaderOnline");
        setSize(750, 750);

		final GLCanvas canvas = new GLCanvas();
		final SpaceInvader space = new SpaceInvader(host, port);
		final FPSAnimator animator = new FPSAnimator(canvas, 60);

		canvas.addGLEventListener(space);		
        canvas.addKeyListener(new CanvasKeyListener(canvas, space));
        this.getContentPane().add(canvas);

        animator.start();
	}

	public static void main(String [] args) {
		IPReceiver.getInstance().start();
		try  {
			IPReceiver.getInstance().join();
		}
		catch(Exception e) {

		}
		
		new Main(IPReceiver.getInstance().getIp(), IPReceiver.getInstance().getPort()).setVisible(true);
	}
}
