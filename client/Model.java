import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import java.util.ArrayList;
import java.io.*;

import javax.media.opengl.awt.GLCanvas;

import java.nio.*;
import org.json.*;
import java.lang.Math;
import java.util.Scanner;
import com.jogamp.common.nio.*;

public class Model {

	public double[] sommets2;
	public int[] index2;
	public float[] normale2;
	
	public DoubleBuffer sommets_fichierBUF;
	public IntBuffer myindex_fichierBUF;
	public FloatBuffer normale_fichierBUF;

	public int sommets_fichierVBO;
	public int myindex_fichierVBO;
	public int normal_fichierVBO;
	public int nbIndex_fichier;


	public Model(GL2 gl, String path, double coef) {
		String jsonString="";
		try {
			Scanner scanner = new Scanner(new File(path));
    	   	jsonString = scanner.useDelimiter("\\Z").next();
    	   	scanner.close();
        }
        catch (Exception e) {
	    	Helper.log(e.toString());
        }
	   	try {

	        JSONObject object = new JSONObject(jsonString);

	        JSONArray arr = object.getJSONArray("vertices");
	        JSONArray arr2 = arr.getJSONObject(0).getJSONArray("values");   	       
   	        JSONArray arrNormal2 = arr.getJSONObject(1).getJSONArray("values");

	        JSONArray arr3 = object.getJSONArray("connectivity");
	        JSONArray arr4 = arr3.getJSONObject(0).getJSONArray("indices");

        	sommets2=new double[arr2.length()];
        	index2=new int[arr4.length()];
        	normale2=new float[arrNormal2.length()];

	        for (int i = 0; i < arr2.length(); i++){
	        	double val = arr2.getDouble(i);
	        	sommets2[i]=val*coef;
	      	}        	
	      	for (int i = 0; i < arr4.length(); i++){  	
        		int val = arr4.getInt(i);
				index2[i]=val;
        	}
        	for (int i=0; i < arrNormal2.length(); ++i){
        		float val =(float) arrNormal2.getDouble(i);
        		normale2[i]=val;
        	}

			sommets_fichierBUF = Buffers.newDirectDoubleBuffer(sommets2);
			myindex_fichierBUF = Buffers.newDirectIntBuffer(index2);
			normale_fichierBUF = Buffers.newDirectFloatBuffer(normale2);

			int[] temp = new int[3];
			gl.glGenBuffers(3, temp, 0);

			sommets_fichierVBO = temp[0];
			gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, sommets_fichierVBO);
			gl.glBufferData(GL2.GL_ARRAY_BUFFER, sommets_fichierBUF.capacity() * Buffers.SIZEOF_DOUBLE,sommets_fichierBUF, GL2.GL_STATIC_DRAW);
			gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
			
			myindex_fichierVBO = temp[1];
			gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, myindex_fichierVBO);
			gl.glBufferData(GL2.GL_ELEMENT_ARRAY_BUFFER, myindex_fichierBUF.capacity() * Buffers.SIZEOF_INT,myindex_fichierBUF, GL2.GL_STATIC_DRAW);
			gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, 0);

			nbIndex_fichier = myindex_fichierBUF.capacity();

			normal_fichierVBO = temp[2];
			gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, normal_fichierVBO);
			gl.glBufferData(GL2.GL_ARRAY_BUFFER, normale_fichierBUF.capacity() * Buffers.SIZEOF_FLOAT,normale_fichierBUF, GL2.GL_STATIC_DRAW);
			gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
	    } 
	    catch (Exception e){
	    	Helper.log(e.toString());
		}    	
	}

	private void loadVBO() {
		
	}
}