import com.jogamp.opengl.util.awt.TextRenderer;
import java.awt.Font;
import java.awt.Color;
import java.util.*;

public final class Console {	

	class Message {
		private String m_value;
		private Color m_color;

		public Message(String value, Color color) {
			m_value = value;
			m_color = color;
		}

		public String getValue() {
			return m_value;
		}

		public Color getColor() {
			return m_color;
		}
	}

	public static final int CONSOLE_FONT_SIZE = 12;
	public static final int CONSOLE_MAX_LINES = 5;

	private HashMap<String, Integer> m_scores;
	private ArrayList<Message> m_messages;
	private TextRenderer m_renderer;
	private SpaceInvader m_game;
	private int m_points;
	private int m_opponentPoints;

	public Console(SpaceInvader game) {
		m_game = game;
		m_scores = new HashMap<String, Integer>();
		m_messages = new ArrayList<Message>();
		m_scores = new HashMap<String, Integer>();
		m_renderer = new TextRenderer(new Font("Verdana", Font.BOLD, CONSOLE_FONT_SIZE));
	}

	public void clearPlayerScores() {
		synchronized(m_scores) {
			m_scores.clear();
		}
	}

	public void setPlayerScore(String id, int score) {
		synchronized(m_scores) {
			m_scores.put(id, score);
		}
	}

	public void addPoint() {
		m_points++;
	}

	public void addOpponentPoint() {
		m_opponentPoints++;
	}

	public void info(String message) {		
		addMessage(message, Color.WHITE);
	}

	public void success(String message) {
		addMessage(message, Color.GREEN);
	}

	public void warning(String message) {
		addMessage(message, Color.ORANGE);
	}

	public void error(String message) {
		addMessage(message, Color.RED);
	}

	public void addMessage(String message, Color color) {
		synchronized(m_messages) {
			m_messages.add(new Message(message, color));
			if(m_messages.size() > CONSOLE_MAX_LINES)
				m_messages.remove(0);
		}
	}

	public void draw(int width, int height) {
		m_renderer.beginRendering(width, height);
		synchronized(m_messages) {
			for(int i = 0; i < m_messages.size(); i++) {
				Message message = m_messages.get(i);	
				m_renderer.setColor(message.getColor());		
				m_renderer.draw(message.getValue(), 5, height - ((i + 1) * CONSOLE_FONT_SIZE) - 5);
			}
		}
		drawPoint(width, height);
		m_renderer.endRendering();
		m_renderer.flush();
	}

	private void drawPoint(int width, int height) {
		m_renderer.setColor(Color.GREEN);		
		m_renderer.draw("Alliés : " + m_points, width - 90, height - CONSOLE_FONT_SIZE - 10);
		m_renderer.setColor(Color.RED);		
		m_renderer.draw("Ennemis : " + m_opponentPoints, width - 90, height - (CONSOLE_FONT_SIZE * 2) - 10);
		m_renderer.setColor(Color.WHITE);	
		m_renderer.draw("[Joueurs]", width - 80, height - (CONSOLE_FONT_SIZE * 3) - 10);
		int i = 4;		
		if(m_game.getEntityMgr() == null)
			return;		
		synchronized(m_scores) {
			for(String pseudo : Helper.sortByValue(m_scores)) {
				m_renderer.draw("[" + m_scores.get(pseudo) + "] " + pseudo, width - 90, height - (CONSOLE_FONT_SIZE * i) - 10);
				i++;
			}
		}
	}
}