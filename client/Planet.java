import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.gl2.GLUT;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import java.util.ArrayList;
import java.io.*;

import javax.media.opengl.awt.GLCanvas;

public class Planet {

	private String m_name;
	private float m_size, m_angle, m_speed;
	private float[] m_rotation, m_color, m_position;
	private ArrayList<Planet> m_satellites;
	private Texture m_texture;
    private float[] m_matrix;

	public Planet(String name, float[] color, float size, float speed, float[] rotation, float[] position) {
		m_name = name;
		m_size = size;
		m_speed = speed;
		m_color = color;
		m_rotation = rotation;
		m_position = position;
        m_matrix = new float[16];
		m_satellites = new ArrayList<Planet>();
	}

    public float getSize() {
        return m_size;
    }

	public Planet addSatellite(Planet planet){
		m_satellites.add(planet);
		return this;
	}

    public float[] getMatrix() {
        return m_matrix;
    }

	public void display(GL2 gl, GLU glu, GLUT glut) {    		
        gl.glPushMatrix();
        gl.glRotated(m_angle += m_speed, m_rotation[0], m_rotation[1], m_rotation[2]);
        gl.glTranslated(m_position[0], m_position[1], m_position[2]);
        gl.glRotated(-m_angle, m_rotation[0], m_rotation[1], m_rotation[2]);            
        gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, m_matrix, 0);
        if(m_texture == null) {
        	gl.glColor3f(m_color[0], m_color[1], m_color[2]);
        	glut.glutSolidSphere(m_size, 20, 8); 
        }
        else {
        	gl.glColor3f(1f, 1f, 1f);
        	
	        m_texture.enable(gl);
	        m_texture.bind(gl);

	        GLUquadric quadric = glu.gluNewQuadric();
	        glu.gluQuadricTexture(quadric, true);
			glu.gluSphere(quadric, m_size, 20, 8);

	        m_texture.disable(gl);
        }            
        for(Planet planet : m_satellites)
        	planet.display(gl, glu, glut);
        gl.glPopMatrix();
	}

	public Planet setTexture(String path) {
		try {
		    m_texture = TextureIO.newTexture(new File(path), true);
		}
		catch (Exception e) {
			System.out.println(e.toString());
		}
		return this;
	}
}