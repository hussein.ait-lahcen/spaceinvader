import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.util.awt.TextRenderer;
import java.awt.Color;

public abstract class DrawableObject extends Entity {

    public DrawableObject(int type, String id, int teamId, int updateInterval) {
        super(type, id, teamId, updateInterval);
    }

    public abstract void display(GLU glu, GLUT glut, GL2 gl, TextRenderer textRenderer);
}    
