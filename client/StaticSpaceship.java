import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.util.awt.TextRenderer;
import java.awt.Color;

public final class StaticSpaceship{

    private Model model;
    private float m_x, m_y, m_z, m_height, m_width, m_direction, m_angle, m_rotx, m_roty, m_rotz;

    public StaticSpaceship(int modelId, float x, float y, float z, float width, float height, float direction) { 
        m_x = x;
        m_y = y;
        m_z = z;
        m_width = width;
        m_height = height;
        m_direction = direction;
        model = ModelManager.getInstance().getModel(modelId);
    }

    public StaticSpaceship setRotation(float angle, float x, float y, float z){
    	m_rotx = x;
    	m_roty = y;
    	m_rotz = z;
    	m_angle = angle;
    	return this;
    }

    public void display(GLU glu, GLUT glut, GL2 gl) {
        gl.glPushMatrix();
        gl.glTranslated(m_x,m_y-(m_height*m_direction),m_z);
        gl.glRotatef(m_angle, m_rotx, m_roty, m_rotz);
        if(m_direction==-1){
            gl.glRotatef(180, 0f,0f,1f);
        }

        //VBO        
        gl.glColor3f(1f, 1f, 1f);
        gl.glScalef(m_width*1f,m_height*1f,m_width*1f);
        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, model.normal_fichierVBO);
        gl.glNormalPointer(GL2.GL_FLOAT,0,0);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, model.sommets_fichierVBO);
        gl.glVertexPointer(3, GL2.GL_DOUBLE, 0, 0);
        gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, model.myindex_fichierVBO);
        gl.glDrawElements(GL2.GL_TRIANGLES, model.nbIndex_fichier, GL2.GL_UNSIGNED_INT, 0);
        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glPopMatrix();
    }	
}