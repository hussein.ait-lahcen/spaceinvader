import java.util.HashMap;
import javax.media.opengl.GL2;

public class ModelManager {

	public static final int SHIP = 0;
	public static final int DESTROYER = 1;
	public static final int BLOCKADE_RUNNER = 2;
	public static final int VENATOR = 3;
	public static final int TIE_INTERCEPTOR = 4;

	private static ModelManager instance;
	private static final Object m_sync = new Object();

	public static ModelManager getInstance() {
		synchronized(m_sync) {
			if(instance ==  null)
				instance = new ModelManager();
			return instance;
		}
	}

	private HashMap<Integer, Model> m_modelById;

	public ModelManager() {
		m_modelById = new HashMap<Integer, Model>();
	}

	public void addModel(GL2 gl, int id, String path, double coef){
		m_modelById.put(id, new Model(gl, path, coef));
	}
 
	public Model getModel(int id) {
		return m_modelById.get(id);
	}
}