import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import com.jogamp.opengl.util.awt.TextRenderer;
import java.awt.Font;
import java.awt.Color;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import java.util.ArrayList;
import java.io.*;

import javax.media.opengl.awt.GLCanvas;

import java.nio.*;
import org.json.*;
import java.lang.Math;
import java.util.Scanner;
import com.jogamp.common.nio.*;

public class SpaceInvader implements GLEventListener {

	public static int Width = 750;
	public static int Height = 750;


// ###############################################################	
	private GLU glu = new GLU();
	private GLUT glut = new GLUT();
	public float cameraX = 0f;
	public float cameraY = 0f;
	public float cameraZ = 100f;
    private EntityManager m_entityManager;
    private SocketClient m_socketClient;
    private TextRenderer m_textRenderer;
    private Console m_console;

    // Soleil

    private Planet sun;
    
    // Liste des objets statiques

    private ArrayList<StaticSpaceship> m_staticSpaceship;

    // Skybox

    private Skybox m_skybox; 

    public SpaceInvader(String host, int port) {
        m_staticSpaceship=new ArrayList<StaticSpaceship>();
        m_entityManager = new EntityManager(this);    
        m_console = new Console(this);
        m_socketClient = new SocketClient(this, host, port);
        m_textRenderer = new TextRenderer(new Font("Verdana", Font.BOLD, 1));
        m_socketClient.start();  
        m_entityManager.start();  
    }

    public EntityManager getEntityMgr() {
        return m_entityManager;
    }

    public Console getConsole() {
    	return m_console;
    }

    public void move(float x, float y, float z) {
        m_socketClient.sendMovement(x, y, z);
    }

    public void fire() {
        m_socketClient.sendFire();
    }

    @Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

        gl.glClear (GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();
        m_skybox.display(gl);
        m_entityManager.display(glu, glut, gl, m_textRenderer);
        for(StaticSpaceship spaceship : m_staticSpaceship){
            spaceship.display(glu, glut, gl);
        }
        sun.display(gl, glu, glut);
        m_console.draw(drawable.getWidth(), drawable.getHeight());      
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {

	}

	@Override
	public void init(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
		GL2 gl = drawable.getGL().getGL2();
		gl.glClearColor (0f, 0f, 0f, 1.0f); 
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glClearDepth(1.0f);	  

		gl.glEnable(GL2.GL_COLOR_MATERIAL);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		gl.glEnable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_LIGHT0);
		gl.glEnable(GL2.GL_NORMALIZE);
        
		final float mat_specular[] =
		{ 1.0f, 1.0f, 1.0f, 1.0f };
		final float light_position[] =
		{ 1.0f, 1.0f, 1.0f, 0.0f };
		final float diffuseMaterial[] =
  		{ 1f, 1f, 1f, 1.0f };

    	gl.glShadeModel(GL2.GL_SMOOTH);
		//gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, diffuseMaterial, 0);
		//gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, mat_specular, 0);
		//gl.glMaterialf(GL2.GL_FRONT, GL2.GL_SHININESS, 50.0f);
		gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, light_position, 0);

    	gl.glColorMaterial(GL2.GL_FRONT, GL2.GL_DIFFUSE);


    	// Import des modèles
    	ModelManager.getInstance().addModel(gl, ModelManager.SHIP, "./model/ship.json", 0.006);
    	ModelManager.getInstance().addModel(gl, ModelManager.DESTROYER, "./model/StarDestroyer.json", 1.0);
        //ModelManager.getInstance().addModel(gl, ModelManager.TIE_INTERCEPTOR, "./model/3DModels/TieInterceptor/TieInterceptor.json", 1.0);
        //ModelManager.getInstance().addModel(gl, ModelManager.VENATOR, "./model/3DModels/Venator/Venator.json", 1.0);
        //ModelManager.getInstance().addModel(gl, ModelManager.BLOCKADE_RUNNER, "./model/3DModels/BlockadeRunner/BlockadeRunner.json", 1.0);

        // Creation des planetes
        sun = new Planet
        (
            "Sun", 
            new float[] { 1.0f, 1.0f, 0.0f }, 
            45f, 
            0.05f, 
            new float[] { 0f, 0f, 1f }, 
            new float[] { 0f, 0f, -90f }
        ).setTexture("texture/sun01.jpg");
        sun.addSatellite(new Planet
            (
                "Mercure",
                new float[] { 1.0f, 0.0f, 0f }, 
                3f, 
                0.5f, 
                new float[] { 0f, 0f, 1f }, 
                new float[] { 60f, 0f, 0f } 
            ).setTexture("texture/planet01.jpg"));
        sun.addSatellite(new Planet
            (
                "Venus",
                new float[] { 1.0f, 0.0f, 0f }, 
                5f, 
                0.7f, 
                new float[] { 0f, 0f, 1f }, 
                new float[] { 90f, 0f, 0f } 
            ).setTexture("texture/planet02.jpg"));
        sun.addSatellite(new Planet
            (
                "Earth",
                new float[] { 0.0f, 0.0f, 1f }, 
                9f, 
                0.5f, 
                new float[] { 0f, 0f, 1f }, 
                new float[] { 120f, 0f, 0f }  
            ).setTexture("/home/aitlahcen/Images/earth.jpg").addSatellite(new Planet
            (
                "Moon",
                new float[] { 0.0f, 0.0f, 1f }, 
                4.5f, 
                1f, 
                new float[] { 0f, 0f, 1f }, 
                new float[] { 15f, 0f, 0f }  
            ).setTexture("texture/planet03.jpg")));

        sun.addSatellite(new Planet
            (
                "Mars",
                new float[] { 0.0f, 0.0f, 1f }, 
                11f, 
                1f, 
                new float[] { 0f, 0f, 1f }, 
                new float[] { 180f, 0f, 0f } 
            ).setTexture("texture/planet04.jpg"));

        m_staticSpaceship.add(new StaticSpaceship(ModelManager.DESTROYER, 50f, 50f, -10f, 5f, 5f, 1)
                .setRotation(90, 1f, 0f, 0f));
        m_staticSpaceship.add(new StaticSpaceship(ModelManager.DESTROYER, -50f, -50f, -15f, 5f, 5f, -1)
                .setRotation(-90, 1f, 0f, 0f));        
	       
        m_skybox = new Skybox("texture/skybox.jpg");
    }

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		// TODO Auto-generated method stub
		GL2 gl = drawable.getGL().getGL2();

		gl.glViewport (0, 0, (int) width, height); 
		gl.glMatrixMode (GL2.GL_PROJECTION);
		gl.glLoadIdentity ();
		glu.gluPerspective(80.0,  width / (float)height, 0.01, 350.0);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
	}   
}
